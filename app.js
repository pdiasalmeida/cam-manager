const express = require('express')
const bodyParser = require('body-parser');
const fs = require('fs');
const { execFile } = require('child_process');
const WebSocketServer = require('websocket').server;

var http = require('http');
var MjpegProxy = require('mjpeg-proxy').MjpegProxy;

// ######################### Global Variables ######################### //
var processes = [];
var videoFiles = [];
var wsClients = [];
const wsProtocols = {'rec-status': true};
// ########## //

// ########## Setup express app (directories and middleware) ########## //
const app = express()

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/scripts'));
app.set('view engine', 'ejs');

app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
// ########## //

// ########### Create proxy streams from our local ip cameras ######### //
app.get('/stream1.jpg',
        new MjpegProxy('http://192.168.1.50/image2').proxyRequest);
app.get('/stream2.jpg',
        new MjpegProxy('http://192.168.1.51/image2').proxyRequest);
/*app.get('/stream3.jpg',
        new MjpegProxy('http://192.168.1.52/image2').proxyRequest);*/
app.get('/stream4.jpg',
        new MjpegProxy('http://192.168.1.53/image2').proxyRequest);
// ########## //

// ############### Handle routes for our express server ############### //
app.get('/', function(req, res) {
    // render our 'index.ejs' to send as request response
    res.render('pages/index')
});

app.post('/', function (req, res) {
    var action = req.body.action;
    var experiment_id = req.body.experimentid;
    var participant_id = req.body.participantid;
    var stream_id = req.body.streamid;
    var timestamp = req.body.timestamp;
    var msg = req.body.msg;

    console.log("Received request with the following parameters:");
    console.log("\t Action: " + action +
        "\n\t Experiment id: " + experiment_id +
        "\n\t Participant id: " + participant_id +
        "\n\t Stream id: " + stream_id +
        "\n\t Timestamp: " + timestamp +
        "\n\t Msg: " + msg +
        "\n"
    );

    if (action === "Record") {
        console.log("Start Recording");
        
        var filename = "";
        if (!timestamp) timestamp = new Date().toISOString();        
        if (experiment_id) {
            experiment_id = experiment_id.substring(0, Math.min(10, experiment_id.length));
            filename += experiment_id + '_'; 
        }
        if (participant_id) {
            participant_id = participant_id.substring(0, Math.min(6, participant_id.length));
            filename += participant_id + '_';
        } 

        filename += timestamp;
        // regular expression to filter invalid chars from the filenames
        filename = filename.replace(/[:<>"\/\\\|\?\*]/g,'-');
        
        switch (stream_id) {
            case 1:
                recordStream(0, 'rtsp://192.168.1.50/video1', 'e:/cctvCam/BackTop/', 'BT_'+filename, 'mkv')
                break;
            case 2:
                recordStream(1, 'rtsp://192.168.1.51/video1', 'e:/cctvCam/LateralLeft/', 'LL_'+filename, 'mkv')
                break;
            case 3:
                //recordStream(2, 'rtsp://192.168.1.52/video1', 'e:/cctvCam/LateralRight/', 'LR_'+filename, 'mkv')
                break;
            case 4:
                recordStream(3, 'rtsp://192.168.1.53/video1', 'e:/cctvCam/FrontTop/', 'FT_'+filename, 'mkv')
                break;
            default:
                recordStream(0, 'rtsp://192.168.1.50/video1', 'e:/cctvCam/BackTop/', 'BT_'+filename, 'mkv')
                recordStream(1, 'rtsp://192.168.1.51/video1', 'e:/cctvCam/LateralLeft/', 'LL_'+filename, 'mkv')
                //recordStream(2, 'rtsp://192.168.1.52/video1', 'e:/cctvCam/LateralRight/', 'LR_'+filename, 'mkv')
                recordStream(3, 'rtsp://192.168.1.53/video1', 'e:/cctvCam/FrontTop/', 'FT_'+filename, 'mkv')
                break;
        }
    } else if (action === "StopRecord") {
        console.log("Stop Recording");
        killProcess(0);
        killProcess(1);
        killProcess(2);
        killProcess(3);
    } else if (action === "RegisterPOI") {
        registerPOI(stream_id, msg);
    }

    // send confirmation to close http request
    res.status(200).send();
});
// ########## //

// ############################# Functions ############################ //
function recordStream(process_id, ip, path, filename, container) {
    if (!processes[process_id]) {
        // option flags for the ffmpeg processes
        var args = [ '-i', ip, 
                    '-acodec', 'copy',
                    '-vcodec', 'copy',
                    path + filename + '.' + container];
        
        // Start ffmpeg process
        // limitação relacionada com o tamanho do buffer associado ao stdout / stderr, estes não podem
        //  exceder {maxBuffer: 1024 * 2000}, ou seja, 2Mb. Isto equivale a ~3h de gravação por ficheiro 
        processes[process_id] = execFile('ffmpeg', args, {maxBuffer: 1024 * 2000}, (error, stdout, stderr) => {
            if (error) {
                console.log('Error ffmpeg process');
                console.log(error);
            }
        });

        // handle ffmpeg process end
        processes[process_id].on('close', (code, signal) => {
            processes[process_id] = null;

            console.log(`child process exited with code ${code} and signal ${signal}`);
            switch (code) {
                case 0:
                    notifyRecSuccess(process_id, path+filename+'.'+container);
                    break;
                default:
                    notifyRecError(process_id, "'Ocorreu um erro, o processo foi terminado.'");
                    break;
            }
        });

        // redirect outputs to the log files
        var logOut = fs.createWriteStream(path + filename + 'Out.log', {flags: 'w'});
        var logErr = fs.createWriteStream(path + filename + 'Err.log', {flags: 'w'});
        
        logOut.on('error', function (err) {
            console.log('Error on log out file');
            console.log(err);
        });
        logErr.on('error', function (err) {
            console.log('Error on log err file');
            console.log(err);
        });
        processes[process_id].stdout.pipe(logOut);
        processes[process_id].stderr.pipe(logErr);

        // save file name for possible pois and notify clients
        console.log('child process created');
        videoFiles[process_id] = path + filename
        notifyChangeRecStatus(process_id, '1');
    } else {
        console.log('Recording already in progress');
    }
}

// sends termination char to process if it is running and closes log files
function killProcess(process_id) {
    if (processes[process_id]) {
        processes[process_id].stdin.write("q\n");
        processes[process_id].stdin.end();
        processes[process_id].stdout.end();
        processes[process_id].stderr.end();
    }
}

// records msg sent in the request in a csv file associated with the video
function registerPOI(stream_id, msg) {
    if (stream_id == 0) {
        for (var stream = 0; stream < videoFiles.length; stream++) {
            let filename = videoFiles[stream] + '.csv'
            fs.appendFile(filename, msg+'\n', function (err) {
                if (err) {
                    console.log('Error registering POI: ' + err);
                } else {
                    console.log('POI registered.');
                }                
            });
        }
    } else if (videoFiles[stream_id-1]) {
        let filename = videoFiles[stream_id-1] + '.csv'
        fs.appendFile(filename, msg+'\n', function (err) {
            if (err) {
                console.log('Error registering POI: ' + err);
            } else {
                console.log('POI for stream ' + stream_id + ' registered.');
            }
        });
    }
}
// ########## //

// ####################### Setup websocket server ##################### //
// msg format for our wsServer will be : '<msg_type>:<other_params(:)>'
// msgs supported:
//     confirmations    => 0:<stream_id>:<msg>
//     errors           => 1:<stream_id>:<msg>
//     recording status => 2:<stream_id>:<rec_status> ('1' is recording '0' is stopped)
//     requests         => 3:<req_id> ('1' is request recording status)
const server = http.createServer(app);
const wsServer = new WebSocketServer({httpServer: server, path: "/ws"});

// handle connections to our web socket server
wsServer.on('request', function(request) {
    // check if connection is according to our restrictions, right now we are only
    //   checking if the 'sec-websocket-protocol' reported in the request is accepted in 
    //   our 'wsProtocols' global variable
    if (!originIsAllowed(request)) {      
        request.reject();
        console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
        return;
    }
    
    // save accepted connections
    var connection = request.accept('rec-status', request.origin);
    console.log((new Date()) + ' Connection accepted.');
    wsClients.push(connection);

    // handle requests on our websocket server, right now we are only listening for 
    //   requests asking for the recording status of all streams  ('3:1')
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            aux = message.utf8Data.split(":");
            if (parseInt(aux[0]) == '3' && parseInt(aux[1]) == '1') {
                notifyRecStatus(this);
            }
        }
    });

    // remove closed connections
    connection.on('close', function(reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
        wsClients = wsClients.filter(function(item) {
            return item !== connection;
        });
    });
});

// function to filter connections to our websocket server
//   we only accept connections that report 'rec-status' as websocket protocol
function originIsAllowed(request) {
    if ('httpRequest' in request && 'headers' in request.httpRequest &&
     'sec-websocket-protocol' in request.httpRequest.headers ) {
        var protocol = request.httpRequest.headers['sec-websocket-protocol'];
        return wsProtocols[protocol];
    }
    return false;
}
// ########## //

// ######### Web socket notification messages for our clients ######### //
// notify given client of the streams that are recording
function notifyRecStatus(connection) {
    for (var i in processes) {
        if (processes[i]) {
            var msg = '2:'+i+':1';
            console.log('Sending the following msg to websocket client: '+msg)
            connection.sendUTF(msg);
        }
    }
}

// notify all clients with the status of our streams
function notifyChangeRecStatus(id, status) {
    for (var client in wsClients) {
        var msg = '2:'+id+':'+status;
        console.log('Sending the following msg to websocket clients: '+msg)
        wsClients[client].sendUTF(msg);
    }
}

// notify all clients of a recording success
function notifyRecSuccess(id, file) {
    for (var client in wsClients) {
        var msg = '0:'+id+':'+'O ficheiro foi gravado em \''+file+'\'';
        console.log('Sending the following msg to websocket clients: '+msg)
        wsClients[client].sendUTF(msg);
    }
}

// notify all clients of a recording error
function notifyRecError(id, text) {
    for (var client in wsClients) {
        var msg = '1:'+id+':'+text;
        console.log('Sending the following msg to websocket clients: '+msg)
        wsClients[client].sendUTF(msg);
    }
}
// ########## //

// ##################### Launch our camera server ##################### //
server.listen(3000, function() {
    console.log('App listening on port 3000\n')
});
// ########## //
