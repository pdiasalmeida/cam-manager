// Model for our nodecam viewer client

var streams = [];
var wsc = null;

// websocket server url
var wsocket_url = 'ws://172.16.31.231:3000/ws'

// ####################### Setup viewer ####################### //
function init() {
    // add onclick functions for the 'Record All' and 'Stop Recording' buttons
    document.getElementById("btnRecordAll").addEventListener("click", function() {
        requestRecord(0);
    }, false);
    document.getElementById("btnStopRecord").addEventListener("click", function() {
        stopRecord();
    }, false);

    // Setup auxiliary data structure to manage stream status info
    setupStream(0, 1, "Stream 1");
    setupStream(1, 2, "Stream 2");
    setupStream(2, 3, "Stream 3");
    setupStream(3, 4, "Stream 4");

    // add onclick functions for the items on the 'Record' dropdown button
    var stop = false;
    for (var streami = 1; !stop; streami++) {
        var btn = document.getElementById("btnRecordStream"+streami)
        if (btn) {
            registerButton(btn, streami)
        } else {
            stop = true;                                        
        }
    }

    // initialize our websocket client
    setupWebSocket();
}

// Setup auxiliary data relating to a stream
function setupStream(index, id, name) {
    streams[index] = {
        name: name,
        id: id,
        uri: "",
        recording: false,
        requestedRec: false,
        requestedStp: false
    };
}

// add onclick functions for the buttons to record individual streams
function registerButton(btn, n) {
    btn.addEventListener("click", function() {
        requestRecord(n);
    }, false);
}

// ###################### Setup WebSocket ##################### //
function setupWebSocket() {
    wsc = new WebSocket(wsocket_url, 'rec-status');

    // start our websocket client by asking for the status of the streams
    wsc.onopen = function () {
        requestRecStatus();
    };

    // handle websocket connection close
    wsc.onclose = function () {
        fireAlert('alert-danger',
         '<strong>Aviso!</strong> Desconectado do servidor.');
    };

    // handle websocket connection error
    wsc.onerror = function (error) {
        console.log('WebSocket Error ' + error);
    };

    // handle messages received on the websocket connection
    wsc.onmessage = function (e) {
        var msg = e.data;
        var arg = msg.split(":");
        // check if message is of type 'recording status' [2:<stream_id>:<rec_status>]
        if (arg[0] == '2') {
            // update stream status in our data structure
            streams[parseInt(arg[1])].recording = (arg[2]=='1');
            // check if stream is recording without this client requesting
            if (arg[2]=='1' && !streams[parseInt(arg[1])].requestedRec) {
                fireAlert('alert-warning',
                 '<strong>Aviso.</strong> Gravação do stream '+ (parseInt(arg[1])+1) +
                  ' foi iniciada por outro cliente.');
            }
            // check if stream stopped recording without this client requesting
            if (arg[2]=='0' && !streams[parseInt(arg[1])].requestedStp) {
                fireAlert('alert-warning',
                 '<strong>Aviso!</strong> Gravação do stream '+ (parseInt(arg[1])+1) +
                  ' foi terminada por outro cliente.');
            }
            
            // update graphic representation of recording status and update data structure
            updateRecordStatus();
            streams[parseInt(arg[1])].requestedRec = false;
            streams[parseInt(arg[1])].requestedStp = false;
        }
        // check if message is of type 'error' [1:<stream_id>:<msg>] and notify user
        else if (arg[0] == '1') {
            fireAlert('alert-danger',
             '<strong>Aviso!</strong> Gravação do stream '+ (parseInt(arg[1])+1) +
              ' terminou com a seguinte mensagem:\n' + arg[2]);

            // update graphic representation of recording status and update data structure
            streams[parseInt(arg[1])].recording = false;
            updateRecordStatus();
        }
        // check if message is of type 'confirmation' [0:<stream_id>:<msg>] and notify user
        else if (arg[0] == '0') {
            fireAlert('alert-success',
             '<strong>Sucesso</strong> na gravação do stream '+ (parseInt(arg[1])+1) + '.\n' +
              arg[2]+':'+arg[3]);

            // update graphic representation of recording status and update data structure
            streams[parseInt(arg[1])].recording = false;
            updateRecordStatus();
        }
    };
}

// ###################### Drawing methods ##################### //
// triggered when there is a resize event on the client window
function handleResize() {
    updateRecordStatus();
}

// updates graphic representation of recording status
function updateRecordStatus() {
    for (var i = 0; streams[i]; i++) {
        if (streams[i].recording) {
            drawFilledCircle(streams[i].id);
        } else {
            clearCanvas(streams[i].id);
        }
    }
}

// draws a red circle on top of the stream video
function drawFilledCircle(stream_id){
    var img = document.getElementById("streamView"+stream_id);
    var cnvs = document.getElementById('cnvStream'+stream_id);

    cnvs.width  = img.clientWidth;
    cnvs.height = img.clientHeight;

    var radius = 10;
    var offset = 2;
    var x = img.clientWidth - (radius+offset);
    var y = radius+offset

    var ctx = cnvs.getContext("2d");
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
    ctx.fillStyle = 'darkred';
    ctx.fill();
}

// clears elements drawed on top of the stream video
function clearCanvas(stream_id) {
    var cnvs = document.getElementById('cnvStream'+stream_id);
    cnvs.getContext("2d").clearRect(0, 0, cnvs.width, cnvs.height);
}

// creates and adds a notification in the viewer client
function fireAlert(alertType, msg) {
    var alert_container = document.getElementById('error');
    var dv = document.createElement("DIV");
    dv.innerHTML = '<div class="alert '+alertType+' alert-dismissable fade in">' +
                     '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                     msg +
                    '</div>';
                
    alert_container.appendChild(dv);
    alert_container.style.display = 'block';
}

// ####################### Data Requests ###################### //
// make 'record' post request to our server
function requestRecord(stream_id) {
    var experiment_id = document.getElementById("iptEid").value;
    var participant_id = document.getElementById("iptPid").value;
    
    var request = new XMLHttpRequest();
    request.open("POST", "/");
    request.setRequestHeader("Content-Type", "application/json");
    request.send(JSON.stringify(
        {
            action: "Record",
            streamid: stream_id,
            experimentid: experiment_id,
            participantid: participant_id
        })
    );

    // update request status in the data structure
    if (stream_id == 0) {
        for (var i in streams) {
            streams[i].requestedRec = true;
        }
    } else {
        streams[stream_id-1].requestedRec = true;
    }

    // handle request response
    request.onreadystatechange = function() {
        // request success
        if (this.readyState == 4 && this.status == 200) {
            if (stream_id == 0) {
                for (var i in streams) {
                    streams[i].recording = true;
                }
            } else {
                streams[stream_id-1].recording = true;
            }
        // request failure
        } else if (this.readyState == 4 && this.status == 0) {
            fireAlert('alert-danger',
             '<strong>Erro!</strong> Não foi recebida resposta do servidor.');
        }
    };
}

// make 'stop recording' post request to our server
function stopRecord() {
    var request = new XMLHttpRequest();
    request.open("POST", "/");
    request.setRequestHeader("Content-Type", "application/json");
    request.send(JSON.stringify(
        {
            action: "StopRecord"
        })
    );

    // update request status in the data structure
    for (var i in streams) {
        streams[i].requestedStp = true;
    }
    
    // handle request response
    request.onreadystatechange = function() {
        // request success
        if (this.readyState == 4 && this.status == 200) {
            for (var i in streams) {
                streams[i].recording = false;
            }
        // request failure
        } else if (this.readyState == 4 && this.status == 0) {
            fireAlert('alert-danger',
             '<strong>Erro!</strong> Não foi recebida resposta do servidor.');
        }
    };
}

// send a request to the websocket server querying the recording status of all streams
function requestRecStatus() {
    if (wsc) {
        wsc.send('3:1');
    }
}
