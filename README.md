Node.js project to manage the viewing and recording of CCTV cameras in a network.

## Requirements
You need to install the following in your environment:

* node
* ffmpeg (accessible as a cmd in your environment)
* node-gyp

## Dependencies

The project has direct dependencies to these npm packages:

* body-parser
* bootstrap
* ejs
* express
* jquery
* websocket
* mjpeg-proxy

These dependencies are installed in the following install step.

## Install

If you are on Windows :( , first you have to setup npm build environment, run the following commands in an admin powershel: 
    
    npm install --global --production windows-build-tools
    npm update

After cloning the repository run `npm install` in the project's base directory in order to setup its dependencies.

## Run
To run the app execute `node app.js` in the project's base directory.

You can then access the application at http://127.0.0.1:3000

## License

This project is licensed under the terms of the **ISC** license.

>You can check out the full license [here](https://gitlab.com/pdiasalmeida/cam-manager/blob/master/LICENSE)
